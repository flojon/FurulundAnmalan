Monban.configure do |config|
  config.user_class = 'AdminUser'
  config.no_login_redirect = :new_admin_session
end