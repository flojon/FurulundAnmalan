Rails.application.routes.draw do
  namespace :admin do
    root 'dashboard#show'
    resource :session, only: [:new, :create, :destroy]
    resources :registrations, only: [:index, :edit, :update, :destroy]
    resources :people, only: [:index]
  end
  
  root 'registrations#new'
  resources :registrations, only: [:new, :create, :show]
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
end
