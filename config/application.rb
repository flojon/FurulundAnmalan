require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Anmalan
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.action_mailer.delivery_method = :smtp
    config.action_mailer.smtp_settings = {
      address: ENV['SMTP_ADDRESS'],
      user_name: ENV['SMTP_USER'],
      password: ENV['SMTP_PASSWORD'],
      port: ENV['SMTP_PORT'],
      authentication: ENV['SMTP_AUTH'],
      domain: ENV['SMTP_DOMAIN']
    }

  end
end
