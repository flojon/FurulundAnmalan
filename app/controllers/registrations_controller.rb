class RegistrationsController < ApplicationController
  layout 'registration'
  helper_method :registration_open?
  helper_method :registration_closed_message

  # GET /registrations/1
  # GET /registrations/1.json
  def show
    @registration = Registration.includes(:people).find_by_hashid(params[:id])
  end

  # GET /registrations/new
  def new
    if registration_open?
      @registration = Registration.new
      6.times { @registration.people.build }
      @age_group_accommodations = AgeGroup.includes(:accommodations)
    end
  end

  # POST /registrations
  # POST /registrations.json
  def create
    @registration = Registration.new(registration_params)

    respond_to do |format|
      if @registration.save
        ApplicationMailer.registration_email(@registration).deliver_later
      
        format.html { redirect_to @registration, notice: 'Registration was successfully created.' }
        format.json { render :show, status: :created, location: @registration }
      else
        (6 - @registration.people.length).times { @registration.people.build } if @registration.people.count < 6
        @age_group_accommodations = AgeGroup.includes(:accommodations)
        format.html { render :new }
        format.json { render json: @registration.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def registration_params
      params.require(:registration).permit(:address, :postcode, :city, :phone, :email, :other, :family, :robin_hood_fond_transaction_sign, :robin_hood_fond_transaction, people_attributes: [:firstname, :lastname, :age_group_id, :age_group_accommodation_id, :accommodation_id])
    end

    def registration_open?
      params["preview"].present? ||
      (!ENV["REGISTRATION_OPEN_DATE"].present? || Time.parse(ENV["REGISTRATION_OPEN_DATE"]) < Time.now) &&
        (!ENV["REGISTRATION_CLOSE_DATE"].present? || Time.parse(ENV["REGISTRATION_CLOSE_DATE"]) > Time.now)
    end

    def registration_closed_message
      if ENV["REGISTRATION_CLOSE_DATE"].present? && Time.parse(ENV["REGISTRATION_CLOSE_DATE"]) < Time.now
        ENV["REGISTRATION_CLOSE_MESSAGE"] || "Anmälan är stängd!"
      elsif ENV["REGISTRATION_OPEN_DATE"].present? && Time.parse(ENV["REGISTRATION_OPEN_DATE"]) > Time.now
        ENV["REGISTRATION_OPEN_MESSAGE"] || "Anmälan har inte öppnat ännu!"
      else
        nil
      end
    end
end
