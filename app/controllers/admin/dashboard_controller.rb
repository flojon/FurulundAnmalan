class Admin::DashboardController < Admin::AdminController
    def show
        @total = Person.count
        @age_groups = Person.joins(:age_group).group(:title).count
        @accommodations = Person.joins(:accommodation).group(:title).count
    end
end 