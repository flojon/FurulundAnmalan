class Admin::RegistrationsController < Admin::AdminController
    before_action :set_registration, only: [:edit, :update, :destroy]
    before_action :set_age_group_accomodations, only: [:edit]

    def index
        @registrations = Registration.includes(people: [:accommodation, :age_group]).order("people.lastname")
    end
    
    # GET /registrations/1/edit
    def edit
    end
    
    # PATCH/PUT /registrations/1
    # PATCH/PUT /registrations/1.json
    def update
        respond_to do |format|
            if @registration.update(registration_params)
                format.html { redirect_to admin_registrations_path, notice: 'Registration was successfully updated.' }
                format.json { render :show, status: :ok, location: @registration }
            else
                set_age_group_accomodations
                format.html { render :edit }
                format.json { render json: @registration.errors, status: :unprocessable_entity }
            end
        end
    end

    # DELETE /registrations/1
    # DELETE /registrations/1.json
    def destroy
        @registration.destroy
        respond_to do |format|
            format.html { redirect_to admin_registrations_path, notice: 'Registration was successfully destroyed.' }
            format.json { head :no_content }
        end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_registration
      @registration = Registration.includes(:people).find_by_hashid(params[:id])
    end
    
    def set_age_group_accomodations
      @age_group_accommodations = AgeGroup.includes(:accommodations)
    end

    def registration_params
      params.require(:registration).permit(:address, :postcode, :city, :phone, :email, :other, :family, people_attributes:
            [:id, :firstname, :lastname, :age_group_id, :age_group_accommodation_id, :accommodation_id])
    end
end