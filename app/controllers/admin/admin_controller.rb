class Admin::AdminController < ApplicationController
    layout 'admin'
    include Monban::ControllerHelpers
    before_action :require_login
end