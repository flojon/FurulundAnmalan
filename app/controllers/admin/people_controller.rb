class Admin::PeopleController < Admin::AdminController
    def index
        @people = Person.where.not(registration_id: nil).includes([:accommodation, :age_group]).order(:lastname)
    end
end
