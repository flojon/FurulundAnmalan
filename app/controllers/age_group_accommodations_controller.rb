class AgeGroupAccommodationsController < ApplicationController
  before_action :set_age_group_accommodation, only: [:show, :edit, :update, :destroy]

  # GET /age_group_accommodations
  # GET /age_group_accommodations.json
  def index
    @age_group_accommodations = AgeGroupAccommodation.all
  end

  # GET /age_group_accommodations/1
  # GET /age_group_accommodations/1.json
  def show
  end

  # GET /age_group_accommodations/new
  def new
    @age_group_accommodation = AgeGroupAccommodation.new
  end

  # GET /age_group_accommodations/1/edit
  def edit
  end

  # POST /age_group_accommodations
  # POST /age_group_accommodations.json
  def create
    @age_group_accommodation = AgeGroupAccommodation.new(age_group_accommodation_params)

    respond_to do |format|
      if @age_group_accommodation.save
        format.html { redirect_to @age_group_accommodation, notice: 'Age group accommodation was successfully created.' }
        format.json { render :show, status: :created, location: @age_group_accommodation }
      else
        format.html { render :new }
        format.json { render json: @age_group_accommodation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /age_group_accommodations/1
  # PATCH/PUT /age_group_accommodations/1.json
  def update
    respond_to do |format|
      if @age_group_accommodation.update(age_group_accommodation_params)
        format.html { redirect_to @age_group_accommodation, notice: 'Age group accommodation was successfully updated.' }
        format.json { render :show, status: :ok, location: @age_group_accommodation }
      else
        format.html { render :edit }
        format.json { render json: @age_group_accommodation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /age_group_accommodations/1
  # DELETE /age_group_accommodations/1.json
  def destroy
    @age_group_accommodation.destroy
    respond_to do |format|
      format.html { redirect_to age_group_accommodations_url, notice: 'Age group accommodation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_age_group_accommodation
      @age_group_accommodation = AgeGroupAccommodation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def age_group_accommodation_params
      params.require(:age_group_accommodation).permit(:age_group_id, :accommodation_id, :price)
    end
end
