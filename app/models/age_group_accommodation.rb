class AgeGroupAccommodation < ApplicationRecord
  belongs_to :age_group
  belongs_to :accommodation
  
  default_scope { includes(:accommodation).order('accommodations.sort_order') }
  
  def title_with_places_left
    accommodation.title_with_places_left + " (#{price} kr)"
  end

  def self.full
    self.select do |item|
      item.accommodation.full
    end
  end
  
  def self.disabled
    self.full.pluck(:id)
  end
end
