class AgeGroup < ApplicationRecord
    has_many :age_group_accommodations
    has_many :accommodations, -> { order 'accommodations.sort_order' }, through: :age_group_accommodations

    def to_s
        title
    end 
end
