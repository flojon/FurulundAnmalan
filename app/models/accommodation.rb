class Accommodation < ApplicationRecord
    has_many :people
    def to_s
        title
    end

    def self.full
        self.select do |accommodation|
            accommodation.full
        end
    end

    def self.disabled
        self.full.pluck(:id)
    end

    def full
        limit && (limit - people.count) < 1
    end

    def title_with_places_left
        if limit
            if (limit - people.count) < 1
                title + " - Fullt!"
            else
                title + " - #{limit-people.count} platser kvar"
            end
         else
            title
         end
    end
    
    def title_with_family_price
        title_with_places_left + " (#{family_price} kr)"
    end
end
