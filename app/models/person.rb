class Person < ApplicationRecord
  belongs_to :registration, inverse_of: :people
  belongs_to :age_group_accommodation, optional: true
  belongs_to :age_group
  belongs_to :accommodation

  validates :firstname, presence: { message: "Förnamn saknas" }
  validates :lastname, presence: { message: "Efternamn saknas" }
  validates :age_group_id, presence: { message: "Välj åldersgrupp" }
  validates :accommodation_id, presence: { message: "Välj boende", if: :registration_family }
  validates :age_group_accommodation_id, presence: { message: "Välj boende", unless: :registration_family }
  
  def name
    self.firstname + " " + self.lastname
  end
  
  def registration_family
    registration.family
  end
end
