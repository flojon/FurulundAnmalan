class Registration < ApplicationRecord
    require 'hashids'
    attr_accessor :robin_hood_fond_transaction_sign
    before_save :update_robin_hood_fond_transactin_sign
    
    @@hash_salt = '2XnfvN3oGqF38vW4HXvR'
    before_validation :set_accommodations
    has_many :people, inverse_of: :registration, :dependent => :delete_all
    accepts_nested_attributes_for :people, reject_if: lambda {|attributes| attributes['firstname'].blank? && attributes['lastname'].blank?}

    validates :address, :postcode, :city, :phone, :email, :people, presence: { message: "Uppgift saknas" }
    validates_associated :people
    validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: "Ogiltig epost", allow_blank: true }
    validates :people, length: { minimum: 1 }
    
    validate :check_accommodation_availabilty, on: :create

    def postcode=(value)
        self[:postcode] = value.remove(" ")
    end

    def postcode
        if self[:postcode] && self[:postcode].length == 5
            self[:postcode][0,3] + " " + self[:postcode][3,2]
        else
            self[:postcode]
        end
    end
    
    def to_param
        return nil unless id 
        hashids = Hashids.new(@@hash_salt, 3)
        hashids.encode id
    end
    
    def self.find_by_hashid(hashid)
        hashids = Hashids.new(@@hash_salt, 3)
        self.find_by_id hashids.decode hashid
    end
    
    def total_price
        if family
            people.first.accommodation.family_price + self.robin_hood_fond_transaction.to_i
        else
            people.includes(:age_group_accommodation).sum("age_group_accommodations.price") + self.robin_hood_fond_transaction.to_i
        end
    end

  private
    def update_robin_hood_fond_transactin_sign
        if self.robin_hood_fond_transaction_sign.to_i < 0
            self.robin_hood_fond_transaction = -self.robin_hood_fond_transaction
        end
    end

    def set_accommodations
        if family
            people.first.age_group_accommodation_id = nil
            people[1..-1].each do |p|
                p.accommodation_id = people.first.accommodation_id
                p.age_group_accommodation_id = nil
            end
        else
            people.each do |p|
                p.accommodation_id = p.age_group_accommodation.accommodation_id if p.age_group_accommodation
            end
        end
    end
    
    def check_accommodation_availabilty
        # Räkna antal för varje boende som har begränsning och kontrollera att det finns tillräckligt med platser kvar
        limits = Accommodation.joins(:people).where.not(limit: nil).group(:accommodation_id, :limit).count().map {|k,v| [k.first, k.second - v]}.to_h
        if family
            acc = people.first.accommodation_id
            if limits.has_key?(acc) && limits[acc] < people.length
                errors[:people] << :invalid
                people.first.errors[:accommodation_id] << "Tyvärr finns det endast #{limits[acc]} platser kvar"
            end
        else
            counts = people.group_by {|p| p.accommodation_id }.map {|k,v| [k,v.length]}.to_h
            people.each do |p|
                acc = p.accommodation_id
                if limits.has_key?(acc)
                    if limits[acc] < 1
                        errors[:people] << :invalid
                        p.errors[:age_group_accommodation_id] << "Tyvärr finns det inga platser kvar"
                    elsif limits[acc] < counts[acc]
                        errors[:people] << :invalid
                        if limits[acc] == 1
                            p.errors[:age_group_accommodation_id] << "Tyvärr finns det endast 1 plats kvar"
                        else
                            p.errors[:age_group_accommodation_id] << "Tyvärr finns det endast #{limits[acc]} platser kvar"
                        end
                    end
                end
            end
        end
    end
end
