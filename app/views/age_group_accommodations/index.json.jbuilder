json.array!(@age_group_accommodations) do |age_group_accommodation|
  json.extract! age_group_accommodation, :id, :age_group_id, :accommodation_id, :price
  json.url age_group_accommodation_url(age_group_accommodation, format: :json)
end
