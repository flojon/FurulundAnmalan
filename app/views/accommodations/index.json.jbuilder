if defined?(@age_group_accommodations)
  json.array!(@age_group_accommodations) do |age_group_accommodation|
    json.extract! age_group_accommodation, :id, :price
    json.extract! age_group_accommodation.accommodation, :title
  end
else
  json.array!(@accommodations) do |accommodation|
    json.extract! accommodation, :id, :title, :description
    json.url accommodation_url(accommodation, format: :json)
  end
end