# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
    $(document).on 'change', '#person_age_group_id', (evt) ->
        $.ajax '/age_groups/' + $('#person_age_group_id option:selected').val() + "/accommodations",
            type: 'GET'
            dataType: 'script'
            error: (jqXHR, textStatus, errorThrown) ->
                console.log("AJAX Error: #{textStatus}")
