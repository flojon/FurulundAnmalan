# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

accommodations = ''
accommodation_prompt = '<option value="">-- Välj boende --</option>'

accommodation_calculate_total_price = ->
    if $("#registration_family").is(":checked")
        el = $("#registration_people_attributes_0_accommodation_id")
    else
        el = $(".person_accommodations")
    sub_total = el
            .find("option:selected")
            .map ->
                val = $(this).text()
                a = val.lastIndexOf('(')
                b = val.lastIndexOf(')')
                i = parseInt(val.substring(a+1, b-3))
                if isNaN(i)
                    return 0
                else
                    return i
            .get()
            .reduce (previousValue, currentValue, currentIndex, arr) ->
                return previousValue + currentValue

    total = sub_total

    if $("#sign_minus").is(":checked")
        $("#registration_robin_hood_fond_transaction").attr("max", sub_total)
        if $("#registration_robin_hood_fond_transaction").val() > sub_total
            $("#registration_robin_hood_fond_transaction").val(sub_total)
        total -= $("#registration_robin_hood_fond_transaction").val()
    else
        $("#registration_robin_hood_fond_transaction").attr("max", null)
        trans = parseInt($("#registration_robin_hood_fond_transaction").val(), 10)
        if isNaN(trans)
            trans = 0
        total += trans

    $("#total").html("#{total} kr").data("sub_total", sub_total)

$(document).on "turbolinks:load", ->
    return unless $(".person_accommodations").length > 0
    
    accommodations = $(".person_accommodations:first").clone().find('option:selected').removeAttr("selected").end().html()

    if $("#registration_family").is(":checked")
        $(".has-error > .person_accommodations").parent().hide()
        $(".person_accommodations").hide()
    else
        $(".has-error > #registration_people_attributes_0_accommodation_id").parent().hide()
        $("#registration_people_attributes_0_accommodation_id").hide()

    $(".person_accommodations").each (index, item) ->
        age_group = $(this).closest(".form-group").find(".person_age_groups option:selected").text()
        options = $(this).find("optgroup[label='#{age_group}']").html()
        if options
            $(this).html(accommodation_prompt+options)
        else
            $(this).html('<option/>')
    accommodation_calculate_total_price()

$(document).on "change", ".person_age_groups", (evt) ->
    if !$("#registration_family").is(":checked")
        age_group = $(this).find("option:selected").text()
        options = $(accommodations).filter("optgroup[label='#{age_group}']").html()
        if options
            $(this).closest(".form-group").find(".person_accommodations").html(accommodation_prompt+options)
        accommodation_calculate_total_price()

$(document).on "change", "#registration_family", (evt) ->
    if $("#registration_family").is(":checked")
        $(".has-error > .person_accommodations").parent().hide()
        $(".person_accommodations").hide()
        $(".has-error > #registration_people_attributes_0_accommodation_id").parent().show()
        $("#registration_people_attributes_0_accommodation_id").show()
    else
        $(".has-error > .person_accommodations").parent().show()
        $(".person_accommodations").show()
        $(".has-error > #registration_people_attributes_0_accommodation_id").parent().hide()
        $("#registration_people_attributes_0_accommodation_id").hide()
    accommodation_calculate_total_price()
        
$(document).on "change", ".person_accommodations", accommodation_calculate_total_price
$(document).on "change", "#registration_people_attributes_0_accommodation_id", accommodation_calculate_total_price
$(document).on "change", "#registration_robin_hood_fond_transaction", accommodation_calculate_total_price
$(document).on "change", "#sign_plus", accommodation_calculate_total_price
$(document).on "change", "#sign_minus", accommodation_calculate_total_price

