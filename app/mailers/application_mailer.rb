class ApplicationMailer < ActionMailer::Base
  default from: ENV['SMTP_USER']
  layout 'mailer'
  
  def registration_email(registration)
    @registration = registration
    mail(to: @registration.email,
        subject: "Välkommen till Furulund- och Öjersjökyrkans församlingshelg")
  end
end
