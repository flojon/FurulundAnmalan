require 'test_helper'

class AgeGroupAccommodationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @age_group_accommodation = age_group_accommodations(:one)
  end

  test "should get index" do
    get age_group_accommodations_url
    assert_response :success
  end

  test "should get new" do
    get new_age_group_accommodation_url
    assert_response :success
  end

  test "should create age_group_accommodation" do
    assert_difference('AgeGroupAccommodation.count') do
      post age_group_accommodations_url, params: { age_group_accommodation: { accommodation_id: @age_group_accommodation.accommodation_id, age_group_id: @age_group_accommodation.age_group_id, price: @age_group_accommodation.price } }
    end

    assert_redirected_to age_group_accommodation_path(AgeGroupAccommodation.last)
  end

  test "should show age_group_accommodation" do
    get age_group_accommodation_url(@age_group_accommodation)
    assert_response :success
  end

  test "should get edit" do
    get edit_age_group_accommodation_url(@age_group_accommodation)
    assert_response :success
  end

  test "should update age_group_accommodation" do
    patch age_group_accommodation_url(@age_group_accommodation), params: { age_group_accommodation: { accommodation_id: @age_group_accommodation.accommodation_id, age_group_id: @age_group_accommodation.age_group_id, price: @age_group_accommodation.price } }
    assert_redirected_to age_group_accommodation_path(@age_group_accommodation)
  end

  test "should destroy age_group_accommodation" do
    assert_difference('AgeGroupAccommodation.count', -1) do
      delete age_group_accommodation_url(@age_group_accommodation)
    end

    assert_redirected_to age_group_accommodations_path
  end
end
