FROM flojon/rails:latest

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY Gemfile .
COPY Gemfile.lock .
RUN bundle install --without test development

COPY . .

ENV RAILS_ENV production 

RUN rails assets:precompile

EXPOSE 3000
CMD ["puma", "-b", "tcp://0.0.0.0:3000", "-e", "production"]
