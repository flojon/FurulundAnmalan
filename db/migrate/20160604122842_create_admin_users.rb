class CreateAdminUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_users do |t|
      t.string :email, null: false, index: true, unique: true
      t.string :password_digest, null: false

      t.timestamps
    end
  end
end
