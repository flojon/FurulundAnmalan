class AddFamilyToRegistrations < ActiveRecord::Migration[5.0]
  def change
    add_column :registrations, :family, :boolean
  end
end
