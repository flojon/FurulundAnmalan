class AddRobinHoodFondTransactionToRegistration < ActiveRecord::Migration[5.0]
  def change
    add_column :registrations, :robin_hood_fond_transaction, :integer
  end
end
