class AddSortOrderToAccommodation < ActiveRecord::Migration[5.0]
  def change
    add_column :accommodations, :sort_order, :integer, size: 1
  end
end
