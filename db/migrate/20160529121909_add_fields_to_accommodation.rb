class AddFieldsToAccommodation < ActiveRecord::Migration[5.0]
  def change
    add_column :accommodations, :limit, :integer, size: 1, null: true
    add_column :accommodations, :family_price, :integer, null: true
  end
end
