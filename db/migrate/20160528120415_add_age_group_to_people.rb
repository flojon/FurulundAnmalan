class AddAgeGroupToPeople < ActiveRecord::Migration[5.0]
  def change
    add_belongs_to :people, :age_group, foreign_key: true
  end
end
