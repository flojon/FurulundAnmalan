class CreateRegistrations < ActiveRecord::Migration[5.0]
  def change
    create_table :registrations do |t|
      t.string :address, limit: 60
      t.string :postcode, limit: 5
      t.string :city, limit: 60
      t.string :phone, limit: 30
      t.string :email, limit: 60
      t.text :other

      t.timestamps
    end
  end
end
