class CreatePeople < ActiveRecord::Migration[5.0]
  def change
    create_table :people do |t|
      t.string :firstname
      t.string :lastname
      t.belongs_to :registration, foreign_key: true
      t.belongs_to :age_group_accommodation, foreign_key: true

      t.timestamps
    end
  end
end
