class CreateAgeGroupAccommodations < ActiveRecord::Migration[5.0]
  def change
    create_table :age_group_accommodations do |t|
      t.belongs_to :age_group, foreign_key: true
      t.belongs_to :accommodation, foreign_key: true
      t.integer :price

      t.timestamps
    end
  end
end
