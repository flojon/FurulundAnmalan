class IncreasePostCodeSize < ActiveRecord::Migration[5.0]
  def change
    change_column :registrations, :postcode, :string, limit: 20
  end
end
