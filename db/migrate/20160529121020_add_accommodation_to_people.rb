class AddAccommodationToPeople < ActiveRecord::Migration[5.0]
  def change
    add_belongs_to :people, :accommodation, foreign_key: true
  end
end
