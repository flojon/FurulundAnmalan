# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170520102313) do

  create_table "accommodations", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "limit"
    t.integer  "family_price"
    t.integer  "sort_order"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",           null: false
    t.string   "password_digest", null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["email"], name: "index_admin_users_on_email"
  end

  create_table "age_group_accommodations", force: :cascade do |t|
    t.integer  "age_group_id"
    t.integer  "accommodation_id"
    t.integer  "price"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["accommodation_id"], name: "index_age_group_accommodations_on_accommodation_id"
    t.index ["age_group_id"], name: "index_age_group_accommodations_on_age_group_id"
  end

  create_table "age_groups", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "people", force: :cascade do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.integer  "registration_id"
    t.integer  "age_group_accommodation_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "age_group_id"
    t.integer  "accommodation_id"
    t.index ["accommodation_id"], name: "index_people_on_accommodation_id"
    t.index ["age_group_accommodation_id"], name: "index_people_on_age_group_accommodation_id"
    t.index ["age_group_id"], name: "index_people_on_age_group_id"
    t.index ["registration_id"], name: "index_people_on_registration_id"
  end

  create_table "registrations", force: :cascade do |t|
    t.string   "address",                     limit: 60
    t.string   "postcode",                    limit: 20
    t.string   "city",                        limit: 60
    t.string   "phone",                       limit: 30
    t.string   "email",                       limit: 60
    t.text     "other"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "family"
    t.integer  "robin_hood_fond_transaction"
  end

end
